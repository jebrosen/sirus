CREATE TABLE pages (
  id INTEGER NOT NULL PRIMARY KEY,
  name TEXT NOT NULL UNIQUE,
  title TEXT NOT NULL,
  parent TEXT NOT NULL DEFAULT "",
  'order' INT NOT NULL DEFAULT 0,
  ctime INT NOT NULL,
  mtime INT NOT NULL,
  content TEXT NOT NULL DEFAULT ""
);

CREATE TABLE posts (
  ctime INTEGER NOT NULL PRIMARY KEY,
  title TEXT NOT NULL,
  slug TEXT NOT NULL,
  mtime INT NOT NULL,
  hidden BOOLEAN NOT NULL DEFAULT 0,
  category TEXT,
  content TEXT NOT NULL
);

CREATE VIEW post_year (ctime, year) AS SELECT
  ctime,
  CAST(strftime('%Y', ctime, 'unixepoch') AS INT)
  FROM posts;

CREATE TABLE users (
  id INTEGER NOT NULL PRIMARY KEY,
  username TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL
);

CREATE TABLE config (
  id INTEGER NOT NULL PRIMARY KEY,
  key TEXT NOT NULL UNIQUE,
  value TEXT NOT NULL
);

CREATE TABLE content (
  id INTEGER NOT NULL PRIMARY KEY,
  path TEXT NOT NULL UNIQUE,
  data BLOB NOT NULL
);
