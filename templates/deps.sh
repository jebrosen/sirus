#/bin/sh

echo "digraph {"
for f in *.html; do
	awk '/{% extends/{gsub("(.*{% extends \"|\" %}.*)",""); print $0}' "$f" | while read -r line; do
		echo "\"${f%.*}\" -> \"${line%.*}\""
	done
done
echo "}"
