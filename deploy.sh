#!/bin/sh
set -eu

binary="sirus"
dest_host="andromeda.jebrosen.com"
dest_file="$binary-next"

cargo build --release --target=x86_64-unknown-linux-musl
scp "target/x86_64-unknown-linux-musl/release/$binary" "${dest_host}:${dest_file}"
ssh -t "$dest_host" sh -c "~/update-sirus"
