# sirus

sirus (SImple RUst Site) 


## Compiling
sirus uses the Rocket web framework and several unstable features,
and therefore must be compiled with a nightly Rust compiler.

sirus also depends on my own forks of Rocket and askama, usually for
open pull requests that have not yet been merged into the upstream
repositories. This policy may change once certain features are released
in new versions of those crates.

## Running and Deploying
sirus has no runtime dependencies aside from libc, and rustc's
musl target can be used to eliminate even that.

sirus expects a Rocket database configuration with the name `sirus` and
a url pointing to the site's sqlite database.  The templates and the
default css files are compiled in and do not need to be deployed
separately.

Example deployment using systemd, for use with a reverse proxy:

```
[Unit]
Description=sirus web server for www.example.com

[Service]
User=http
ExecStart=/usr/local/bin/sirus
Environment=RUST_LOG=warn RUST_BACKTRACE=1 ROCKET_ADDRESS="127.0.0.1"
Environment=ROCKET_DATABASES="[{name=\"sirus\",url=\"/srv/www.example.com/sirus.db\"}]"
Restart=on-failure

[Install]
WantedBy=default.target
```

## Administration
By default, a user with the name `admin` and password `admin` will
be created.  The administration page is located at `/admin/`.
Here, users can be added and deleted and additional content files can
be added and deleted.

Any files uploaded as content can be referenced from anywhere within
the site; they are served at `/static/<filename>`. Files uploaded
as content take precedence over default content files with the same name.

## Default Content
The default content is defined in `src/statics/mod.rs`:

* sirus.css: base layout and theme style definitions
* upload.js: file upload code for the site administration page
* feed-icon.png: an RSS feed icon used for the blog post listing page
* robots.txt: blocks off /admin from conforming web crawlers

* site.css: blank by default; linked by the base template
* logo.png: site logo used at the top of the page
* favicon.ico: site icon
