use rocket::request::Request;
use rocket::response::{Responder, Response, Result};

/// A [Responder](rocket::response::Responder) that sets the max-age of
/// a response for caching.  Delegates the remainder of the response to
/// the wrapped responder.
pub struct CacheControl<R>(pub i32, pub R);

impl<R: Responder<'r>> Responder<'r> for CacheControl<R> {
    fn respond_to(self, req: &Request) -> Result<'r> {
        Response::build_from(self.1.respond_to(req)?)
            .raw_header("Cache-Control", format!("max-age={}", self.0))
            .ok()
    }
}
