use std::io::{empty, Cursor};
use std::str::FromStr;

use etag::EntityTag;

use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::hyper::header;
use rocket::http::{Method, Status};
use rocket::request::Request;
use rocket::response::{Body, Response};

/// A Fairing for rocket that adds an `ETag` header to responses for
/// better cache handling. It additionally checks the `If-None-Match`
/// header and responds with a `304 Not Modified` status if the
/// requested resource is identical by ETag comparison.
pub struct ETag(u64);

impl ETag {
    /// The default maximum length of a response to be checksummed and
    /// verified by the `ETag` fairing.
    pub const DEFAULT_MAX_LENGTH: u64 = 2 * 1024 * 1024;

    /// Returns a new `ETag` Fairing with the given maximum length.
    /// Responses will only be checked and verified if they are
    /// shorter than this length.
    pub fn new(max_length: u64) -> ETag {
        ETag(max_length)
    }

    /// Retrieves the maximum length of a response that `self` will
    /// generate or verify an ETag for.
    pub fn max_length(&self) -> u64 {
        self.0
    }
}

impl Default for ETag {
    /// Returns a new `ETag` Fairing with the default maximum length
    /// defined by [ETag::DEFAULT_MAX_LENGTH].
    fn default() -> ETag {
        ETag::new(2 * 1024 * 1024)
    }
}

impl Fairing for ETag {
    fn info(&self) -> Info {
        Info {
            name: "ETag",
            kind: Kind::Response,
        }
    }

    fn on_response(&self, request: &Request, response: &mut Response) {
        // don't change an ETag if it's already present
        if response.headers().contains("ETag") {
            return;
        }

        // ETag should only be added to GET/HEAD requests
        match request.method() {
            Method::Get | Method::Head => (),
            _ => return,
        };

        // limit length for ETag generation
        match response.body() {
            Some(Body::Sized(_, length)) if length < self.max_length() => (),
            _ => return,
        };

        // etag the body (pulls the body out of the response)
        let body_bytes = response.body_bytes();
        let (etag, length) = match &body_bytes {
            Some(b) => (EntityTag::from_hash(b), b.len()),
            _ => return,
        };

        // put body back into the response
        response.set_sized_body(Cursor::new(body_bytes.unwrap()));

        // set the etag header on the response
        let etag_hdr = header::EntityTag::new(true, etag.tag().to_string());
        response.set_header(header::ETag(etag_hdr.clone()));

        // check for an If-None-Match
        if let Some(match_str) = request.headers().get_one("If-None-Match") {
            // check that the client asks for either '*' or the matching etag
            let any_match = match_str.split(',').map(str::trim).any(|val| {
                if val == "*" {
                    return true;
                }
                let match_etag = header::EntityTag::from_str(val);
                match_etag.map(|mt| mt.weak_eq(&etag_hdr)).unwrap_or(false)
            });

            if any_match {
                // set 304 status with no body
                response.set_status(Status::NotModified);
                response.set_raw_body(Body::Sized(Box::new(empty()), length as u64));
            }
        }
    }
}
