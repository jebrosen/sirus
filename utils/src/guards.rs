use std::io::{self, ErrorKind, Read};

use rocket::data::{self, Data, FromData};
use rocket::request::Request;
use rocket::Outcome;

/// Defines a request guard with a given `name`, which corresponds to
/// a limit `key` in the Rocket configuration. If the `key` is not present,
/// the guard will forward unless a `default` was also specified.
///
/// The request guard implements [Deref](std::ops::Deref) to retrieve the value, a `u64`.
///
/// # Example
/// ```rust
/// # #![feature(plugin, decl_macro)]
/// # #![plugin(rocket_codegen)]
/// # extern crate rocket;
/// # #[macro_use] extern crate sirus_utils;
/// # use rocket::Data;
/// #
/// limit_guard!(UploadLimit, "upload", 2 * 1024 * 1024); // 2MiB
///
/// #[put("/upload.txt", data = "<data>")]
/// fn upload(limit: UploadLimit, data: Data) -> Result<(), ()> {
///     // ...
///     println!("Limit is: {}", *limit);
///     # Ok(())
/// }
/// ```
#[macro_export]
macro_rules! limit_guard {
    ($name:ident, $key:expr) => {
        pub struct $name(u64);
        impl ::std::ops::Deref for $name {
            type Target = u64;
            fn deref(&self) -> &u64 {
                &self.0
            }
        }
        impl<'a, 'r> ::rocket::request::FromRequest<'a, 'r> for $name {
            type Error = ();

            fn from_request(
                request: &'a ::rocket::Request<'r>,
            ) -> ::rocket::request::Outcome<Self, Self::Error> {
                match request.limits().get($key) {
                    Some(limit) => ::rocket::Outcome::Success($name(limit)),
                    None => Outcome::Forward,
                }
            }
        }
    };
    ($name:ident, $key:expr, $default:expr) => {
        pub struct $name(u64);
        impl ::std::ops::Deref for $name {
            type Target = u64;
            fn deref(&self) -> &u64 {
                &self.0
            }
        }
        impl<'a, 'r> ::rocket::request::FromRequest<'a, 'r> for $name {
            type Error = ();

            fn from_request(
                request: &'a ::rocket::Request<'r>,
            ) -> ::rocket::request::Outcome<Self, Self::Error> {
                ::rocket::Outcome::Success($name(request.limits().get($key).unwrap_or($default)))
            }
        }
    };
}

/// A data guard that can be accessed to consume up to a limited
/// number of bytes of the incoming data stream.
pub struct LimitedData {
    size: Option<usize>,
    data: Data,
}

impl LimitedData {
    /// Read and return up to `max` bytes. `Err([LimitedDataError])` is
    /// returned if more than `max` bytes were provided.
    ///
    /// The Content-Length header, if present, is used as a hint for
    /// the buffer size and is checked against `max` before data is read.
    pub fn consume(self, max: usize) -> Result<Vec<u8>, LimitedDataError> {
        if let Some(size) = self.size {
            if size > max {
                return Err(LimitedDataError::TooBig);
            }
        }

        let mut stream = self.data.open();
        let mut result = Vec::with_capacity(self.size.unwrap_or(0));

        let mut total: usize = 0;

        let mut buf = [0; 4096];
        loop {
            let count = match stream.read(&mut buf) {
                Ok(0) => break,
                Ok(n) => n,
                Err(ref e) if e.kind() == ErrorKind::Interrupted => continue,
                Err(e) => Err(e)?,
            };

            total = match total.checked_add(count) {
                None => return Err(LimitedDataError::TooBig),
                Some(t) if t > max => return Err(LimitedDataError::TooBig),
                Some(t) => t,
            };
            result.extend(&buf[0..count]);
        }

        Ok(result)
    }
}

impl FromData for LimitedData {
    type Error = ();

    fn from_data(req: &Request, data: Data) -> data::Outcome<Self, Self::Error> {
        let size = req
            .headers()
            .get_one("Content-Length")
            .and_then(|h| h.parse::<usize>().ok());

        Outcome::Success(LimitedData { size, data })
    }
}

/// The error type for [LimitedData]
pub enum LimitedDataError {
    /// The maximum number of bytes to return was exceeded
    TooBig,
    /// An [io::Error](I/O Error) occurred while reading the data stream
    IoError(io::Error),
}

impl From<io::Error> for LimitedDataError {
    fn from(e: io::Error) -> LimitedDataError {
        LimitedDataError::IoError(e)
    }
}
