//! This crate contains several general-purpose building blocks for
//! Rocket applications. Currently the following are provided:
//!
//! ## Fairings
//!
//! * [ETag]: adds and checks ETags for better cache handling
//!
//! ## Request Guards
//!
//! * [limit_guard]: a macro that creates request guards for configured limits
//! * [LimitedData]: a data guard that will read only a
//!   certain number of bytes from the incoming data stream
//!
//! ## Responders
//!
//! * [CacheControl]: sets the `max-age` of a response

#![feature(in_band_lifetimes)]
extern crate etag;
extern crate rocket;

mod fairings;
mod guards;
mod responders;

pub use fairings::*;
pub use guards::*;
pub use responders::*;
