#![feature(rust_2018_preview)]
#![feature(plugin, decl_macro, custom_derive, use_extern_macros)]
#![plugin(rocket_codegen)]
#![allow(proc_macro_derive_resolution_fallback)]
#![cfg_attr(
    feature = "cargo-clippy",
    allow(
        needless_pass_by_value,
        suspicious_else_formatting,
        print_literal
    )
)]
#![cfg_attr(debug_assertions, feature(alloc_system, allocator_api))]
#[cfg(debug_assertions)]
extern crate alloc_system;

#[cfg(debug_assertions)]
#[global_allocator]
static ALLOCATOR: alloc_system::System = alloc_system::System;

// TODO: figure out how to not use macro_use for these
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

mod blog_atom;
mod guards;
mod routes;
mod schema;
mod statics;
mod storage;
mod templates;

use log::{error, log};
use rocket::fairing::AdHoc;

use crate::storage::{DieselConn, DieselConnPool};

const AUTH_TIMEOUT: i64 = 24 * 3600; // one day

pub fn get_time() -> i64 {
    chrono::Utc::now().timestamp()
}

fn main() {
    rocket::ignite()
        .mount("/", routes::site::routes())
        .mount("/admin", routes::admin::routes())
        .mount("/blog", routes::blog::routes())
        .catch(routes::catchers::catchers())
        .attach(DieselConn::fairing())
        .attach(AdHoc::on_attach(|rocket| {
            let mut success = false;
            {
                let pool: &DieselConnPool =
                    rocket.state().expect("diesel database connection pool");
                if let Ok(conn) = pool.get() {
                    match storage::init_diesel_db(conn) {
                        Ok(()) => success = true,
                        Err(e) => error!("could not initialize database. {:?}", e),
                    }
                }
            }

            if success {
                Ok(rocket)
            } else {
                Err(rocket)
            }
        }))
        .attach(sirus_utils::ETag::default())
        .launch();
}
