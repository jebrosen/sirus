use rocket::request::Request;

use crate::storage::DieselConn;
use crate::templates::{err_404, E500, SitePage};

pub fn catchers() -> Vec<::rocket::Catcher> {
    catchers![e404, e500]
}

#[catch(404)]
pub fn e404(req: &Request) -> SitePage {
    let pagetree = req
        .guard::<DieselConn>()
        .succeeded()
        .and_then(|conn| conn.get_pagetree(false).ok())
        .unwrap_or_default();
    err_404(None, pagetree)
}

#[catch(500)]
pub fn e500() -> E500 {
    E500::new(String::default())
}
