use std::borrow::Cow;
use std::error::Error;
use std::ffi::OsStr;
use std::fmt;
use std::path::PathBuf;

use rocket::http::uri::Uri;
use rocket::http::{ContentType, RawStr, Status};
use rocket::request::{Form, Request};
use rocket::response::status::Custom;
use rocket::response::{Content, Redirect, Responder, Response};
use sirus_utils::CacheControl;

use crate::get_time;
use crate::guards::*;
use crate::statics;
use crate::storage::{DieselConn, NewPage, Page, StorageError};
use crate::templates::{err_404, SitePage, SitePageEdit};

const CACHE_TIME_STATIC: i32 = 7 * 24 * 3600; // one week
const CACHE_TIME_DYNAMIC: i32 = 24 * 3600; // one day

pub fn routes() -> Vec<::rocket::Route> {
    routes![static_file, index, view, editor, editor_redir, edit]
}

#[derive(Debug)]
enum SiteError {
    MissingField(Cow<'static, str>),
    DeleteFailed(String),
    InsertFailed(String),
    StorageError(StorageError),
}

impl Error for SiteError {
    fn cause(&self) -> Option<&dyn Error> {
        match self {
            SiteError::StorageError(e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for SiteError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            SiteError::MissingField(s) => write!(f, "Missing required field: '{}'", s),
            SiteError::DeleteFailed(s) => write!(f, "Failed to delete '{}'", s),
            SiteError::InsertFailed(s) => write!(f, "Failed to insert '{}'", s),
            SiteError::StorageError(e) => e.fmt(f),
        }
    }
}

impl From<StorageError> for SiteError {
    fn from(e: StorageError) -> Self {
        SiteError::StorageError(e)
    }
}

#[get("/")]
pub fn index(conn: DieselConn, admin: Option<Admin>) -> Result<Custom<SitePage>, StorageError> {
    view("home".into(), conn, admin)
}

enum StaticResponse<'a> {
    Compiled(&'a [u8], ContentType),
    Database(Vec<u8>, ContentType),
}

impl Responder<'a> for StaticResponse<'a> {
    fn respond_to(self, req: &Request) -> Result<Response<'a>, Status> {
        match self {
            StaticResponse::Compiled(bytes, ct) => {
                // TODO: don't use to_vec, once possible
                CacheControl(CACHE_TIME_STATIC, Content(ct, bytes.to_vec())).respond_to(req)
            }
            StaticResponse::Database(bytes, ct) => {
                CacheControl(CACHE_TIME_DYNAMIC, Content(ct, bytes)).respond_to(req)
            }
        }
    }
}

#[get("/static/<path..>")]
pub fn static_file(
    path: PathBuf,
    conn: DieselConn,
) -> Result<Option<impl Responder<'static>>, StorageError> {
    let path_str = path.to_string_lossy();

    Ok(match conn.get_content(&path_str)? {
        Some(bytes) => {
            let ct = path
                .extension()
                .and_then(OsStr::to_str)
                .and_then(ContentType::from_extension)
                .unwrap_or(ContentType::Binary);
            Some(StaticResponse::Database(bytes, ct))
        }
        None => statics::get_static_data(&path_str)
            .map(|(bytes, ct)| StaticResponse::Compiled(bytes, ct)),
    })
}

#[get("/<name>", rank = 3)]
pub fn view(
    name: String,
    conn: DieselConn,
    admin: Option<Admin>,
) -> Result<Custom<SitePage>, StorageError> {
    let is_admin = admin.is_some();
    let pagetree = conn.get_pagetree(is_admin)?;
    let copyright = conn.get_config("site.copyright")?.unwrap_or_default();

    match conn.get_page(&name, is_admin)? {
        Some(page) => Ok(Custom(
            Status::Ok,
            SitePage::new(page, pagetree, is_admin, copyright),
        )),
        None => Ok(Custom(Status::NotFound, err_404(Some(&name), pagetree))),
    }
}

#[get("/<name>/edit", rank = 1)]
pub fn editor(_admin: Admin, name: String, conn: DieselConn) -> Result<SitePageEdit, StorageError> {
    let pagetree = conn.get_pagetree(true)?;

    let page = conn.get_page(&name, true)?.unwrap_or_else(|| Page {
        id: -1,
        name: name.clone(),
        title: name,
        parent: "".into(),
        order: 0,
        ctime: 0,
        mtime: 0,
        content: "".into(),
    });

    Ok(SitePageEdit::new(page, pagetree, true, String::default()))
}

#[get("/<_name>/edit", rank = 2)]
pub fn editor_redir(_name: &RawStr, uri: &Uri) -> Redirect {
    Redirect::to(format!("/admin/login?dest={}", uri))
}

#[derive(FromForm)]
pub struct PageEditor {
    name: String,
    title: String,
    order: i32,
    parent: String,
    content: String,
    action: Option<String>,
}

#[post("/<old_name>/edit", data = "<form>")]
fn edit(
    _admin: Admin,
    old_name: String,
    form: Form<PageEditor>,
    conn: DieselConn,
) -> Result<Redirect, SiteError> {
    let PageEditor {
        name,
        title,
        order,
        parent,
        content,
        action,
    } = form.into_inner();

    if name.is_empty() {
        return Err(SiteError::MissingField("name".into()));
    }

    let delete_id: Option<String>;
    let upsert_page: Option<NewPage>;
    let redirect_to;

    if action.as_ref().map(String::as_str) == Some("delete") {
        delete_id = Some(old_name.clone());
        upsert_page = None;
        redirect_to = uri!(index);
    } else {
        let now = get_time();
        let mut new_page = NewPage {
            name: &name,
            title: &title,
            order,
            parent: Some(&parent),
            content: &content,
            mtime: now,
            ctime: now,
        };

        if let Some(old) = conn.get_page(&old_name, true)? {
            new_page.ctime = old.ctime;
            if old.name != new_page.name {
                delete_id = Some(old.name.clone());
            } else {
                delete_id = None;
            }
        } else {
            delete_id = None;
        }

        redirect_to = uri!(view: name = &name);
        upsert_page = Some(new_page);
    };

    if let Some(upsert_page) = upsert_page {
        conn.upsert_page(upsert_page)
            .or_else(|_| Err(SiteError::InsertFailed(name.clone())))?;
    }

    if let Some(delete_id) = delete_id {
        conn.delete_page(&delete_id)
            .or_else(|_| Err(SiteError::DeleteFailed(delete_id)))?;
    }

    Ok(Redirect::to(redirect_to))
}
