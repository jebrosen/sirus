use std::error::Error;
use std::fmt;

use chrono::{Datelike, TimeZone, Utc};
use rocket::http::uri::Uri;
use rocket::http::{RawStr, Status};
use rocket::request::Form;
use rocket::response::status::Custom;
use rocket::response::{Content, Redirect};

use crate::blog_atom::atom_generate;
use crate::get_time;
use crate::guards::*;
use crate::storage::{DieselConn, NewPost, Post, StorageError};
use crate::templates::{AtomFeed, BlogPost, BlogPostEdit, BlogPostList};

pub fn routes() -> Vec<::rocket::Route> {
    routes![index, index_year, view, atom, editor, editor_redir, edit]
}

#[derive(Debug)]
enum BlogError {
    DeleteFailed(String),
    InsertFailed(String),
    StorageError(StorageError),
}

impl Error for BlogError {
    fn cause(&self) -> Option<&dyn Error> {
        match self {
            BlogError::StorageError(e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for BlogError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            BlogError::DeleteFailed(s) => write!(f, "Failed to delete '{}'", s),
            BlogError::InsertFailed(s) => write!(f, "Failed to insert '{}'", s),
            BlogError::StorageError(e) => e.fmt(f),
        }
    }
}

impl From<StorageError> for BlogError {
    fn from(e: StorageError) -> Self {
        BlogError::StorageError(e)
    }
}

fn year_from_timestamp(ts: i64) -> i32 {
    Utc.timestamp(ts, 0).year()
}

pub fn url_fragment(post: &Post) -> String {
    format!("{}/{}", post.ctime, post.slug)
}

pub fn make_slug(title: &str) -> String {
    if title.is_empty() {
        return "untitled".into();
    }

    let mut slug = String::with_capacity(title.len());
    let mut hyphen = false;
    for c in title.chars().flat_map(char::to_lowercase) {
        // ignore quotes
        if c == '\'' || c == '"' {
            continue;
        }

        // only care about alphanumeric characters
        if (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') {
            // also add a hyphen if there were non-alphanumeric characters before
            if hyphen {
                slug.push('-');
                hyphen = false;
            }
            slug.push(c);
        } else {
            hyphen = true;
        }
    }
    slug
}

#[get("/")]
fn index(admin: Option<Admin>, conn: DieselConn) -> Result<BlogPostList, StorageError> {
    index_year(year_from_timestamp(get_time()), admin, conn)
}

#[get("/<year>")]
fn index_year(
    year: i32,
    admin: Option<Admin>,
    conn: DieselConn,
) -> Result<BlogPostList, StorageError> {
    let is_admin = admin.is_some();
    let copyright = conn.get_config("site.copyright")?.unwrap_or_default();
    let posts = conn.get_posts_by_year(year, is_admin)?;
    let (prev_year, next_year) = conn.get_adjacent_years(year, is_admin)?;

    Ok(BlogPostList::new(
        year, prev_year, next_year, is_admin, posts, copyright,
    ))
}

#[get("/<ctime>/<_slug>")]
fn view(
    ctime: i64,
    _slug: String,
    admin: Option<Admin>,
    conn: DieselConn,
) -> Result<Custom<BlogPost>, StorageError> {
    // must match ctime, slug
    let is_admin = admin.is_some();

    let year: i32;
    let post: Post;
    let status: Status;

    let maybe_post = conn.get_post(ctime, is_admin)?;
    if let Some(p) = maybe_post {
        year = year_from_timestamp(ctime);
        post = p;
        status = Status::Ok;
    } else {
        // not found for post
        year = year_from_timestamp(get_time());
        post = Post {
            ctime: 0,
            title: "Post Not Found".into(),
            slug: "".into(),
            mtime: 0,
            hidden: false,
            category: None,
            content: "The blog post you requested does not exist".into(),
        };
        status = Status::NotFound;
    }

    let year_posts = conn.get_posts_by_year(year, is_admin)?;
    let (prev_year, next_year) = conn.get_adjacent_years(year, is_admin)?;

    let copyright = conn.get_config("site.copyright")?.unwrap_or_default();

    Ok(Custom(
        status,
        BlogPost::new(post, prev_year, next_year, is_admin, year_posts, copyright),
    ))
}

#[get("/atom.xml")]
fn atom(conn: DieselConn, host: HostHeader) -> Result<Content<AtomFeed>, StorageError> {
    atom_generate(conn, host.0)
}

#[get("/<ctime>/<_slug>/edit", rank = 1)]
fn editor(
    _admin: Admin,
    ctime: i64,
    _slug: &RawStr,
    conn: DieselConn,
) -> Result<BlogPostEdit, StorageError> {
    let post = conn.get_post(ctime, true)?.unwrap_or_else(|| Post {
        ctime: 0,
        title: "New Post".into(),
        slug: "".into(),
        mtime: 0,
        hidden: false,
        category: None,
        content: "".into(),
    });

    let year = year_from_timestamp(ctime);
    let year_posts = conn.get_posts_by_year(year, true)?;
    let (prev_year, next_year) = conn.get_adjacent_years(year, true)?;

    Ok(BlogPostEdit::new(
        post,
        prev_year,
        next_year,
        true,
        year_posts,
        String::default(),
    ))
}

#[get("/<_ctime>/<_slug>/edit", rank = 2)]
fn editor_redir(_ctime: String, _slug: String, uri: &Uri) -> Redirect {
    Redirect::to(format!("/admin/login?dest={}", uri))
}

#[derive(FromForm)]
pub struct PostEditor {
    title: String,
    slug: String,
    hidden: Option<String>,
    category: String,
    content: String,
    action: Option<String>,
}

#[post("/<ctime>/<_slug>/edit", data = "<form>")]
fn edit(
    _admin: Admin,
    ctime: i64,
    _slug: String,
    form: Form<PostEditor>,
    conn: DieselConn,
) -> Result<Redirect, BlogError> {
    let form = form.into_inner();

    if form.action.as_ref().map_or(false, |s| s == "delete") {
        return match conn.delete_post(ctime) {
            Ok(_) => Ok(Redirect::to(uri!("/blog", index))),
            Err(_) => Err(BlogError::DeleteFailed(form.title)),
        };
    }

    let now = get_time();
    let ctime = match conn.get_post(ctime, true)? {
        Some(_existing) => ctime,
        None => now,
    };
    let slug = if form.slug == "" {
        make_slug(&form.title)
    } else {
        form.slug
    };
    let hidden = form.hidden.as_ref().map_or(false, |s| s == "hidden");

    let post = NewPost {
        ctime,
        title: &form.title,
        slug: &slug,
        hidden,
        mtime: now,
        category: Some(&form.category),
        content: &form.content,
    };

    let redirect = Redirect::to(uri!("/blog", view: ctime = post.ctime, _slug = &slug));
    match conn.upsert_post(post) {
        Ok(_) => Ok(redirect),
        Err(_) => Err(BlogError::InsertFailed(form.title.clone())),
    }
}
