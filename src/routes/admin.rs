use std::error::Error;
use std::fmt;
use std::io;
use std::path::PathBuf;

use log::{log, warn};
use rocket::http::{
    uri::{Segments, Uri},
    Cookie, Cookies, Status,
};
use rocket::request::{FlashMessage, Form};
use rocket::response::{status::Custom, Flash, Redirect};
use rocket::FromFormValue;
use sirus_utils::{LimitedData, LimitedDataError};

use crate::get_time;
use crate::guards::*;
use crate::storage::{DieselConn, StorageError};
use crate::templates::{AdminHome, AdminLogin};

pub fn routes() -> Vec<::rocket::Route> {
    routes![
        login_get,
        login_post,
        login_post_to,
        logout,
        admin_get,
        admin_redir,
        admin_post,
        admin_upload_file,
        admin_upload_file_anon,
    ]
}

#[derive(Debug)]
pub enum AdminError {
    InvalidInput,
    UploadTooBig,
    IoError(io::Error),
    StorageError(StorageError),
}

impl Error for AdminError {
    fn cause(&self) -> Option<&dyn Error> {
        match self {
            AdminError::IoError(e) => Some(e),
            AdminError::StorageError(e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for AdminError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            AdminError::InvalidInput => write!(f, "Invalid input"),
            AdminError::UploadTooBig => write!(f, "Upload too big"),
            AdminError::IoError(inner) => inner.fmt(f),
            AdminError::StorageError(inner) => inner.fmt(f),
        }
    }
}

impl From<io::Error> for AdminError {
    fn from(err: io::Error) -> Self {
        AdminError::IoError(err)
    }
}
impl From<StorageError> for AdminError {
    fn from(err: StorageError) -> Self {
        AdminError::StorageError(err)
    }
}
impl From<LimitedDataError> for AdminError {
    fn from(err: LimitedDataError) -> Self {
        match err {
            LimitedDataError::TooBig => AdminError::UploadTooBig,
            LimitedDataError::IoError(e) => AdminError::IoError(e),
        }
    }
}

#[get("/login")]
fn login_get(flash: Option<FlashMessage>) -> AdminLogin {
    let error = match flash.as_ref() {
        Some(f) if f.name() == "error" => Some(f.msg().to_string()),
        Some(f) => {
            warn!("Unhandled flash message type: {}", f.name());
            None
        }
        _ => None,
    };

    AdminLogin::new(error)
}

#[derive(FromForm)]
pub struct LoginForm {
    username: String,
    password: String,
}

#[derive(FromForm)]
struct LoginRedir {
    pub dest: String,
}

#[post("/login", data = "<form>")]
fn login_post(
    form: Form<LoginForm>,
    cookies: Cookies,
    uri: &Uri,
    conn: DieselConn,
) -> Result<Redirect, Flash<Redirect>> {
    login_post_to(
        form,
        LoginRedir {
            dest: uri!("/admin", admin_get).to_string(),
        },
        cookies,
        uri,
        conn,
    )
}

#[post("/login?<redir>", data = "<form>")]
fn login_post_to(
    form: Form<LoginForm>,
    redir: LoginRedir,
    mut cookies: Cookies,
    uri: &Uri,
    conn: DieselConn,
) -> Result<Redirect, Flash<Redirect>> {
    let LoginForm { username, password } = form.into_inner();
    match conn.check_password(&username, &password) {
        Ok(true) => {
            cookies.add_private(Cookie::new(
                "session",
                format!("{};{}", username, get_time()),
            ));
            Ok(Redirect::to(redir.dest))
        }
        _ => Err(Flash::error(
            Redirect::to(uri.to_string()),
            "Incorrect username or password",
        )),
    }
}

#[get("/logout")]
fn logout(mut cookies: Cookies) -> Redirect {
    cookies.remove_private(Cookie::named("session"));
    Redirect::to(uri!(::crate::routes::site::index))
}

#[get("/", rank = 1)]
fn admin_get(
    admin: Admin,
    conn: DieselConn,
    flash: Option<FlashMessage>,
) -> Result<AdminHome, StorageError> {
    let msg = flash.map(|f| (f.name().to_string(), f.msg().to_string()));

    let users = conn.get_users()?;
    let content_paths = conn.get_content_paths()?;

    Ok(AdminHome::new(msg, admin.username, users, content_paths))
}

#[get("/", rank = 2)]
fn admin_redir() -> Redirect {
    Redirect::to(uri!("/admin", login_get))
}

#[derive(FromFormValue)]
enum AdminAction {
    AddUser,
    DeleteUser,
    DeleteContent,
}

#[derive(FromForm)]
pub struct AdminForm {
    action: AdminAction,
    new_username: String,
    new_password: String,
    selected_username: String,
    selected_content: String,
}

#[post("/", data = "<form>")]
fn admin_post(
    _admin: Admin,
    conn: DieselConn,
    form: Form<AdminForm>,
) -> Result<Redirect, Flash<Redirect>> {
    let AdminForm {
        action,
        new_username,
        new_password,
        selected_username,
        selected_content,
    } = form.into_inner();

    match action {
        AdminAction::AddUser => add_user(conn, new_username, new_password),
        AdminAction::DeleteUser => conn
            .delete_user(&selected_username)
            .map_err(AdminError::from),
        AdminAction::DeleteContent => conn
            .delete_content(&selected_content)
            .map_err(AdminError::from),
    }.map_err(|ae| Flash::error(Redirect::to(uri!("/admin", admin_get)), format!("{}", ae)))?;

    Ok(Redirect::to(uri!("/admin", admin_get)))
}

fn add_user(conn: DieselConn, username: String, password: String) -> Result<(), AdminError> {
    if username.is_empty() || password.is_empty() {
        return Err(AdminError::InvalidInput);
    }

    conn.create_user(&username, &password)?;
    Ok(())
}

#[put("/upload/<path..>", data = "<data>", rank = 1)]
fn admin_upload_file(
    _admin: Admin,
    conn: DieselConn,
    upload_limit: ContentUploadLimit,
    path: PathBuf,
    data: LimitedData,
) -> Result<(), AdminError> {
    let path_str = path.to_str().ok_or(AdminError::InvalidInput)?;
    let bytes = data.consume(*upload_limit as usize)?;
    conn.upsert_content(path_str, &bytes)?;
    Ok(())
}

#[put("/upload/<_path..>", rank = 2)]
fn admin_upload_file_anon(_path: Segments) -> Custom<()> {
    Custom(Status::Unauthorized, ())
}
