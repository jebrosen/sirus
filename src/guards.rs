use rocket::http::Cookies;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request};
use sirus_utils::limit_guard;

use crate::{get_time, storage::DieselConn, AUTH_TIMEOUT};

pub struct User(pub String);
impl FromRequest<'_, '_> for User {
    type Error = ();

    fn from_request(request: &Request) -> request::Outcome<Self, Self::Error> {
        let mut cookies = request.guard::<Cookies>()?;
        if let Some(cookie) = cookies.get_private("session") {
            let mut split = cookie.value().split(';');
            if let Some(username) = split.next() {
                let conn = request.guard::<DieselConn>()?;
                if let Ok(Some(user)) = conn.get_user(username) {
                    if let Some(timestamp) = split.next().and_then(|s| s.parse::<i64>().ok()) {
                        if get_time() - timestamp < AUTH_TIMEOUT {
                            return Outcome::Success(User(user));
                        }
                    }
                }
            }
        };

        Outcome::Forward(())
    }
}

pub struct Admin {
    pub username: String,
}
impl FromRequest<'_, '_> for Admin {
    type Error = ();

    fn from_request(request: &Request) -> request::Outcome<Self, Self::Error> {
        match request.guard::<User>() {
            Outcome::Success(User(username)) => Outcome::Success(Self { username }),
            Outcome::Forward(_) => Outcome::Forward(()),
            Outcome::Failure(f) => Outcome::Failure(f),
        }
    }
}

pub struct HostHeader<'a>(pub &'a str);
impl FromRequest<'a, '_> for HostHeader<'a> {
    type Error = ();

    fn from_request(request: &'a Request) -> request::Outcome<Self, Self::Error> {
        match request.headers().get_one("Host") {
            Some(h) => Outcome::Success(HostHeader(h)),
            None => Outcome::Forward(()),
        }
    }
}

limit_guard!(ContentUploadLimit, "content_upload", 2 * 1024 * 1024);
