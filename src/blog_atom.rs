use chrono::{DateTime, TimeZone, Utc};
use rocket::http::ContentType;
use rocket::response::Content;

use crate::routes::blog::url_fragment;
use crate::storage::{DieselConn, StorageError};
use crate::templates::{AtomFeed, AtomPost};

fn tag(authority: &str, updated: &DateTime<Utc>, path: &str) -> String {
    format!("tag:{},{}:{}", authority, updated.format("%Y-%m-%d"), path)
}

fn strip_port(host: &str) -> &str {
    host.split(':').nth(0).unwrap_or(host)
}

pub fn atom_generate(conn: DieselConn, host: &str) -> Result<Content<AtomFeed>, StorageError> {
    let origin = format!("https://{}", host);
    let posts = conn.get_posts_latest(20)?;
    let host_name = strip_port(host);

    let updated = match posts.get(0) {
        Some(p) => Utc.timestamp(p.mtime, 0),
        None => Utc::now(),
    };

    let entries: Vec<_> = posts
        .into_iter()
        .map(|post| {
            let url = url_fragment(&post);
            let updated = Utc.timestamp(post.mtime, 0);
            AtomPost {
                id: tag(host_name, &updated, &url),
                title: post.title,
                updated,
                href: format!("{}/blog/{}", origin, url),
                content: post.content,
            }
        })
        .collect();

    let feed = AtomFeed {
        id: format!("{}/", origin),
        title: conn
            .get_config_or_default("blog.title", "Untitled Blog")
            .into(),
        icon: format!("{}/favicon.ico", origin),
        updated,
        href: format!("{}/blog/atom.xml", origin),
        author: conn
            .get_config_or_default("blog.author", "Unknown Author")
            .into(),
        entries,
    };

    Ok(Content(ContentType::new("application", "atom+xml"), feed))
}
