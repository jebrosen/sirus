use std::borrow::Cow;

use diesel::prelude::*;

use super::*;
use crate::schema::config;

impl DieselConn {
    pub fn get_config(&self, key: &str) -> Result<Option<String>, StorageError> {
        config::table
            .filter(config::key.eq(key))
            .select(config::value)
            .first(&self.0)
            .optional()
            .map_err(From::from)
    }

    pub fn get_config_or_default(&self, key: &str, default: &'a str) -> Cow<'a, str> {
        match self.get_config(key).unwrap_or_default() {
            Some(s) => s.into(),
            None => default.into(),
        }
    }
}
