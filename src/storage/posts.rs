use super::*;
use crate::schema::post_year;
use crate::schema::posts;

#[derive(Queryable)]
pub struct Post {
    pub ctime: i64,
    pub title: String,
    pub slug: String,
    pub mtime: i64,
    pub hidden: bool,
    pub category: Option<String>,
    pub content: String,
}

#[derive(Insertable)]
#[table_name = "posts"]
pub struct NewPost<'a> {
    pub ctime: i64,
    pub title: &'a str,
    pub slug: &'a str,
    pub mtime: i64,
    pub hidden: bool,
    pub category: Option<&'a str>,
    pub content: &'a str,
}

impl DieselConn {
    pub fn get_post(&self, ctime: i64, include_hidden: bool) -> Result<Option<Post>, StorageError> {
        posts::table
            .filter(posts::ctime.eq(ctime))
            .filter(posts::hidden.eq(false).or(include_hidden))
            .first(&self.0)
            .optional()
            .map_err(From::from)
    }

    pub fn get_posts_by_year(
        &self,
        year: i32,
        show_hidden: bool,
    ) -> Result<Vec<Post>, StorageError> {
        posts::table
            .inner_join(post_year::table)
            .filter(post_year::year.eq(year))
            .filter(posts::hidden.eq(false).or(show_hidden))
            .order_by(posts::ctime.asc())
            .select(posts::table::all_columns())
            .load(&self.0)
            .map_err(From::from)
    }

    pub fn get_posts_latest(&self, count: i64) -> Result<Vec<Post>, StorageError> {
        posts::table
            .filter(posts::hidden.eq(false))
            .order_by(posts::ctime.desc())
            .limit(count)
            .load(&self.0)
            .map_err(From::from)
    }

    pub fn get_adjacent_years(
        &self,
        year: i32,
        show_hidden: bool,
    ) -> Result<(Option<i32>, Option<i32>), StorageError> {
        let prev: Option<i32> = posts::table
            .inner_join(post_year::table)
            .filter(post_year::year.lt(year))
            .filter(posts::hidden.eq(false).or(show_hidden))
            .order_by(post_year::year.desc())
            .select(post_year::year)
            .first(&self.0)
            .optional()?;

        let next: Option<i32> = posts::table
            .inner_join(post_year::table)
            .filter(post_year::year.gt(year))
            .filter(posts::hidden.eq(false).or(show_hidden))
            .order_by(post_year::year.asc())
            .select(post_year::year)
            .first(&self.0)
            .optional()?;

        Ok((prev, next))
    }

    pub fn upsert_post(&self, post: NewPost) -> Result<(), StorageError> {
        diesel::replace_into(posts::table)
            .values(post)
            .execute(&self.0)?;
        Ok(())
    }

    pub fn delete_post(&self, ctime: i64) -> Result<(), StorageError> {
        diesel::delete(posts::table.filter(posts::ctime.eq(ctime))).execute(&self.0)?;
        Ok(())
    }
}
