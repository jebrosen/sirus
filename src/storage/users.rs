use bcrypt;
use diesel::associations::HasTable;
use diesel::prelude::*;

use super::*;
use crate::schema::users;

#[derive(Insertable)]
#[table_name = "users"]
struct NewUser<'a> {
    pub username: &'a str,
    pub password: &'a str,
}

impl DieselConn {
    pub fn get_users(&self) -> Result<Vec<String>, StorageError> {
        users::table
            .select(users::username)
            .load(&self.0)
            .map_err(From::from)
    }

    pub fn get_user(&self, username: &str) -> Result<Option<String>, StorageError> {
        users::table
            .filter(users::username.eq(username))
            .select(users::username)
            .first(&self.0)
            .optional()
            .map_err(From::from)
    }

    pub fn create_user(&self, username: &str, password: &str) -> Result<(), StorageError> {
        let hashed = bcrypt::hash(password, bcrypt::DEFAULT_COST).expect("hashed password");

        diesel::insert_into(users::table)
            .values(NewUser {
                username,
                password: &hashed,
            })
            .execute(&self.0)?;
        Ok(())
    }

    pub fn delete_user(&self, username: &str) -> Result<(), StorageError> {
        diesel::delete(users::table.filter(users::username.eq(username))).execute(&self.0)?;
        Ok(())
    }

    pub fn check_password(&self, username: &str, password: &str) -> Result<bool, StorageError> {
        let hash: String = users::table
            .filter(users::username.eq(username))
            .select(users::password)
            .first(&self.0)?;

        bcrypt::verify(password, &hash).map_err(|_| StorageError::AuthError)
    }
}
