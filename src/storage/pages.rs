use std::cmp::Ordering;

use diesel::prelude::*;
use log::warn;

use super::*;
use crate::schema::pages;

#[derive(Queryable)]
pub struct Page {
    pub id: i32,
    pub name: String,
    pub title: String,
    pub parent: String,
    pub order: i32,
    pub ctime: i64,
    pub mtime: i64,
    pub content: String,
}

#[derive(PartialEq, Eq, Ord)]
pub struct NavPage {
    pub name: String,
    pub title: String,
    pub parent: String,
    pub order: i32,
    pub pages: Vec<NavPage>,
}

impl PartialOrd for NavPage {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let o = self.order.cmp(&other.order);
        if o != Ordering::Equal {
            return Some(o);
        }
        Some(self.title.to_lowercase().cmp(&other.title.to_lowercase()))
    }
}

pub fn make_pagetree(pages: Vec<NavPage>, include_hidden: bool) -> Vec<NavPage> {
    let (mut parents, mut children): (Vec<_>, Vec<_>) =
        pages.into_iter().partition(|p| p.parent == "");

    if !include_hidden {
        children.retain(|p| p.order != 0);
    }

    let mut orphans: Vec<NavPage> = Vec::new();
    for page in children {
        if let Some(par) = parents.iter_mut().find(|par| par.name == page.parent) {
            &mut par.pages
        } else {
            &mut orphans
        }.push(page)
    }

    if !orphans.is_empty() {
        warn!(
            "orphan pages detected: {}",
            orphans
                .iter()
                .map(|o| o.name.as_str())
                .collect::<Vec<_>>()
                .join(", ")
        );
        parents.extend(orphans);
    }

    for par in &mut parents {
        par.pages.sort();
    }

    if !include_hidden {
        parents.retain(|p| p.order != 0);
    }
    parents.sort();
    parents
}

#[derive(Insertable)]
#[table_name = "pages"]
pub struct NewPage<'a> {
    pub name: &'a str,
    pub title: &'a str,
    pub parent: Option<&'a str>,
    pub order: i32,
    pub ctime: i64,
    pub mtime: i64,
    pub content: &'a str,
}

impl DieselConn {
    pub fn get_page(&self, name: &str, include_hidden: bool) -> Result<Option<Page>, StorageError> {
        pages::table
            .filter(pages::name.eq(name))
            .filter(pages::order.ne(0).or(include_hidden))
            .first(&self.0)
            .optional()
            .map_err(From::from)
    }

    pub fn get_navpages(&self) -> Result<Vec<NavPage>, StorageError> {
        let data = pages::table
            .select((pages::name, pages::title, pages::parent, pages::order))
            .get_results(&self.0)?;

        Ok(data
            .into_iter()
            .map(|(name, title, parent, order)| NavPage {
                name,
                title,
                parent,
                order,
                pages: vec![],
            })
            .collect())
    }

    pub fn get_pagetree(&self, include_hidden: bool) -> Result<Vec<NavPage>, StorageError> {
        Ok(make_pagetree(self.get_navpages()?, include_hidden))
    }

    pub fn upsert_page(&self, page: NewPage) -> Result<(), StorageError> {
        diesel::replace_into(pages::table)
            .values(page)
            .execute(&self.0)?;
        Ok(())
    }

    pub fn delete_page(&self, name: &str) -> Result<(), StorageError> {
        diesel::delete(pages::table)
            .filter(pages::name.eq(name))
            .execute(&self.0)?;
        Ok(())
    }
}
