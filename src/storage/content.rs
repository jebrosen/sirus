use diesel::prelude::*;

use super::*;
use crate::schema::content;

#[derive(Insertable)]
#[table_name = "content"]
struct NewContent<'a> {
    pub path: &'a str,
    pub data: &'a [u8],
}

impl DieselConn {
    pub fn get_content(&self, path: &str) -> Result<Option<Vec<u8>>, StorageError> {
        content::table
            .filter(content::path.eq(path))
            .select(content::data)
            .first(&self.0)
            .optional()
            .map_err(From::from)
    }

    pub fn upsert_content(&self, path: &str, data: &[u8]) -> Result<(), StorageError> {
        diesel::replace_into(content::table)
            .values(NewContent { path, data })
            .execute(&self.0)?;
        Ok(())
    }

    pub fn get_content_paths(&self) -> Result<Vec<String>, StorageError> {
        content::table
            .select(content::path)
            .load(&self.0)
            .map_err(From::from)
    }

    pub fn delete_content(&self, path: &str) -> Result<(), StorageError> {
        diesel::delete(content::table.filter(content::path.eq(path))).execute(&self.0)?;
        Ok(())
    }
}
