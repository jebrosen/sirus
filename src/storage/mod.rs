#![allow(unused_imports)]

use std::convert::From;
use std::error::Error;
use std::fmt;

use diesel;
use diesel::prelude::*;
use diesel::r2d2::PoolError;
use log::{info, error, log, warn};
use rocket::{error_, log_};
use rocket_contrib::database;

use crate::schema;

mod config;
pub use self::config::*;
mod content;
pub use self::content::*;
mod pages;
pub use self::pages::*;
mod posts;
pub use self::posts::*;
mod users;
pub use self::users::*;

#[derive(Debug)]
pub enum StorageError {
    DieselError(diesel::result::Error),
    MigrationError(diesel_migrations::RunMigrationsError),
    AuthError,
}

impl Error for StorageError {
    fn cause(&self) -> Option<&dyn Error> {
        match self {
            StorageError::DieselError(e) => Some(e),
            StorageError::MigrationError(e) => Some(e),
            _ => None,
        }
    }
}

impl fmt::Display for StorageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            StorageError::DieselError(e) => e.fmt(f),
            StorageError::MigrationError(e) => e.fmt(f),
            StorageError::AuthError => write!(f, "Invalid credentials or processing error"),
        }
    }
}

impl From<diesel::result::Error> for StorageError {
    fn from(e: diesel::result::Error) -> Self {
        StorageError::DieselError(e)
    }
}

impl From<diesel_migrations::RunMigrationsError> for StorageError {
    fn from(e: diesel_migrations::RunMigrationsError) -> Self {
        StorageError::MigrationError(e)
    }
}

#[database("sirus")]
pub struct DieselConn(diesel::SqliteConnection);

impl DieselConnPool {
    pub fn get(&self) -> Result<DieselConn, PoolError> {
        self.0.get().map(DieselConn)
    }
}

embed_migrations!();

pub fn init_diesel_db(conn: DieselConn) -> Result<(), StorageError> {
    info!("Running database migrations");
    embedded_migrations::run(&conn.0)?;

    let count: i64 = schema::users::table.count().get_result(&conn.0)?;
    if count < 1 {
        warn!("No users exist. Creating default user.");
        conn.create_user(DEFAULT_USER, DEFAULT_PASS)?;
    }
    Ok(())
}

const DEFAULT_USER: &str = "admin";
const DEFAULT_PASS: &str = "admin";
