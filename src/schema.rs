table! {
    config (id) {
        id -> Integer,
        key -> Text,
        value -> Text,
    }
}

table! {
    content (id) {
        id -> Integer,
        path -> Text,
        data -> Binary,
    }
}

table! {
    pages (id) {
        id -> Integer,
        name -> Text,
        title -> Text,
        parent -> Text,
        order -> Integer,
        ctime -> BigInt,
        mtime -> BigInt,
        content -> Text,
    }
}

table! {
    posts (ctime) {
        ctime -> BigInt,
        title -> Text,
        slug -> Text,
        mtime -> BigInt,
        hidden -> Bool,
        category -> Nullable<Text>,
        content -> Text,
    }
}

table! {
    post_year (ctime) {
        ctime -> BigInt,
        year -> Integer,
    }
}

table! {
    users (id) {
        id -> Integer,
        username -> Text,
        password -> Text,
    }
}

allow_tables_to_appear_in_same_query!(config, content, pages, post_year, posts, users,);

joinable!(post_year -> posts (ctime));
