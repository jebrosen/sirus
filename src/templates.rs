use askama::Template;
use chrono::{DateTime, TimeZone, Utc};

use crate::storage::{NavPage, Page, Post};

#[derive(Template)]
#[template(path = "base.html")]
pub struct Base {
    copyright: String,
}

#[derive(Template)]
#[template(path = "500.html")]
pub struct E500 {
    _parent: Base,
    detail: String,
}

impl E500 {
    pub fn new(detail: String) -> E500 {
        E500 {
            _parent: Base {
                copyright: String::default(),
            },
            detail,
        }
    }
}

#[derive(Template)]
#[template(path = "site.html")]
pub struct SitePage {
    _parent: Base,
    page: Page,
    ctimef: String,
    mtimef: String,
    page_tree: Vec<NavPage>,
    is_admin: bool,
}

impl SitePage {
    pub fn new(page: Page, page_tree: Vec<NavPage>, is_admin: bool, copyright: String) -> SitePage {
        let ctimef = Utc.timestamp(page.ctime, 0).format("%F").to_string();
        let mtimef = Utc.timestamp(page.mtime, 0).format("%F").to_string();
        SitePage {
            _parent: Base { copyright },
            page,
            ctimef,
            mtimef,
            page_tree,
            is_admin,
        }
    }
}

#[derive(Template)]
#[template(path = "site_page_edit.html")]
pub struct SitePageEdit {
    _parent: SitePage,
}

impl SitePageEdit {
    pub fn new(
        page: Page,
        page_tree: Vec<NavPage>,
        is_admin: bool,
        copyright: String,
    ) -> SitePageEdit {
        SitePageEdit {
            _parent: SitePage::new(page, page_tree, is_admin, copyright),
        }
    }
}

#[derive(Template)]
#[template(path = "blog_post_list.html")]
pub struct BlogPostList {
    _parent: Base,
    year: i32,
    prev_year: Option<i32>,
    next_year: Option<i32>,
    is_admin: bool,
    posts: Vec<Post>,
    current: i64,
}

impl BlogPostList {
    pub fn new(
        year: i32,
        prev_year: Option<i32>,
        next_year: Option<i32>,
        is_admin: bool,
        posts: Vec<Post>,
        copyright: String,
    ) -> BlogPostList {
        BlogPostList {
            _parent: Base { copyright },
            year,
            prev_year,
            next_year,
            is_admin,
            posts,
            current: 0,
        }
    }
}

#[derive(Template)]
#[template(path = "blog_post.html")]
pub struct BlogPost {
    _parent: Base,
    post: Post,
    ctimef: String,
    mtimef: String,
    prev_year: Option<i32>,
    next_year: Option<i32>,
    is_admin: bool,
    posts: Vec<Post>,
    current: i64,
}

impl BlogPost {
    pub fn new(
        post: Post,
        prev_year: Option<i32>,
        next_year: Option<i32>,
        is_admin: bool,
        posts: Vec<Post>,
        copyright: String,
    ) -> BlogPost {
        let ctimef = Utc.timestamp(post.ctime, 0).format("%F").to_string();
        let mtimef = Utc.timestamp(post.mtime, 0).format("%F").to_string();
        BlogPost {
            _parent: Base { copyright },
            ctimef,
            mtimef,
            prev_year,
            next_year,
            is_admin,
            posts,
            current: post.ctime,
            post,
        }
    }
}

#[derive(Template)]
#[template(path = "blog_post_edit.html")]
pub struct BlogPostEdit {
    _parent: BlogPost,
}

impl BlogPostEdit {
    pub fn new(
        post: Post,
        prev_year: Option<i32>,
        next_year: Option<i32>,
        is_admin: bool,
        posts: Vec<Post>,
        copyright: String,
    ) -> BlogPostEdit {
        BlogPostEdit {
            _parent: BlogPost::new(post, prev_year, next_year, is_admin, posts, copyright),
        }
    }
}

#[derive(Template)]
#[template(path = "admin_login.html")]
pub struct AdminLogin {
    _parent: Base,
    msg_error: Option<String>,
}

impl AdminLogin {
    pub fn new(msg_error: Option<String>) -> AdminLogin {
        AdminLogin {
            _parent: Base {
                copyright: String::default(),
            },
            msg_error,
        }
    }
}

#[derive(Template)]
#[template(path = "admin_home.html")]
pub struct AdminHome {
    _parent: Base,
    msg: Option<(String, String)>,
    current_username: String,
    users: Vec<String>,
    content_paths: Vec<String>,
}

impl AdminHome {
    pub fn new(
        msg: Option<(String, String)>,
        current_username: String,
        users: Vec<String>,
        content_paths: Vec<String>,
    ) -> AdminHome {
        AdminHome {
            _parent: Base {
                copyright: String::default(),
            },
            msg,
            current_username,
            users,
            content_paths,
        }
    }
}

#[derive(Template)]
#[template(path = "blog_atom.xml")]
pub struct AtomFeed {
    pub id: String,
    pub title: String,
    pub icon: String,
    pub updated: DateTime<Utc>,
    pub href: String,
    pub author: String,
    pub entries: Vec<AtomPost>,
}

pub struct AtomPost {
    pub id: String,
    pub title: String,
    pub updated: DateTime<Utc>,
    pub href: String,
    pub content: String,
}

pub fn err_404(name: Option<&str>, pagetree: Vec<NavPage>) -> SitePage {
    let (name, label) = if let Some(name) = name {
        (name.into(), format!(" ({})", name))
    } else {
        ("".into(), "".into())
    };

    SitePage::new(
        Page {
            id: -1,
            name,
            title: "Page Not Found".into(),
            parent: "home".into(),
            order: 0,
            ctime: 0,
            mtime: 0,
            content: format!("The page you requested{} does not exist", label),
        },
        pagetree,
        false,
        String::default(),
    )
}
