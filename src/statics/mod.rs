use rocket::http::ContentType;

const SIRUS_CSS: &[u8] = include_bytes!("sirus.css");
const UPLOAD_JS: &[u8] = include_bytes!("upload.js");
const FEED_ICON_PNG: &[u8] = include_bytes!("feed-icon.png");
const LOGO_PNG: &[u8] = include_bytes!("logo.png");
const FAVICON_ICO: &[u8] = include_bytes!("favicon.ico");
const ROBOTS_TXT: &[u8] = include_bytes!("robots.txt");
const SITE_CSS: &[u8] = b"";

pub fn get_static_data(key: &str) -> Option<(&'static [u8], ContentType)> {
    match key {
        "sirus.css" => Some((SIRUS_CSS, ContentType::CSS)),
        "upload.js" => Some((UPLOAD_JS, ContentType::JavaScript)),
        "site.css" => Some((SITE_CSS, ContentType::CSS)),
        "feed-icon.png" => Some((FEED_ICON_PNG, ContentType::PNG)),
        "logo.png" => Some((LOGO_PNG, ContentType::PNG)),
        "favicon.ico" => Some((FAVICON_ICO, ContentType::Binary)),
        "robots.txt" => Some((ROBOTS_TXT, ContentType::Plain)),
        _ => None,
    }
}
