function appendUploadLog(cls, text) {
	const uploadLog = document.getElementById("upload_log");
	const p = document.createElement("p");
	p.className = cls;
	p.innerText = text;
	uploadLog.appendChild(p);
}

function uploadContent() {
	const newPath = document.getElementsByName("new_path")[0].value;
	const newData = document.getElementsByName("new_data")[0].files[0];

	if (newPath && newData) {
		fetch("/admin/upload/" + newPath, {
			method: "PUT",
			body: newData,
		}).then(function(response) {
			if (response.ok) {
				appendUploadLog("success", "Uploaded " + newPath);
			} else {
				appendUploadLog("error", "Failed to upload " + newPath + ": " + response.statusText);
			}
		}).catch(function(err) {
			appendUploadLog("error", err);
		});
	}
}

document.getElementById("upload_button").addEventListener("click", uploadContent);
